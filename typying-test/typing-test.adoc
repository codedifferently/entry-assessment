:icons: fonts

= Typing Aptitude Test

* Begin by taking a basic typing test to measure your typing speed and accuracy. You will be able to take a 45 second practice test before the actual test. Click this link: https://www.aptitude-test.com/free-aptitude-test/typing-test/[Typing Test] to begin the test. The test will take 1 minute.

* Once the test is completed, you should see a screen like the image below:

image::typing-result.png[typing result screen]