:icons: fonts

= HTML Basics

. What does HTML stand for? *Hypertext Markup Language*

. How many headings in HTML are there?   *6*

. All content displayed on a web page is nested inside of what element? *Body*

. What attributes points to the location of an image?   *src*

. Which element is used for hyperlinks? *<a>*

. Which element creates a numbered list?    *<ol>*

. True or false: The <title> element's content appears on a web page. *FALSE*

. What is the first declaration of an HTML document? *<!DOCTYPE HTML>*

. What are the three languages of the web? *HTML, CSS, JS*

. What element is used to connect the HTML document with CSS? *<link>*

. What attribute and value will open a link in a new tab?   *target="_blank"*

. What symbol denotes a closing tag? */*

. Which attribute specifies an alternate text for an image, if it cannot be displayed? *alt*

. Name an element that is used to create sections within an HTML document. *<section> or <div>*
