:icons: fonts

= HTML Basics

. What does HTML stand for?

. How many headings in HTML are there?

. All content displayed on a web page is nested inside of what element?

. What attributes points to the location of an image?

. Which element is used for hyperlinks?

. Which element creates a numbered list?

. True or false: The <title> element's content appears on a web page.

. What is the first declaration of an HTML document?

. What are the three languages of the web?

. What element is used to connect the HTML document with CSS? 

. What attribute and value will open a link in a new tab?

. What symbol denotes a closing tag?

. Which attribute specifies an alternate text for an image, if it cannot be displayed?

. Name an element that is used to create sections within an HTML document.
