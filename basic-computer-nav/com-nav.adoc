:icons: font

= Basic Computer Navigation Questions

This section will test your ability to navigate through a computer or laptop. 

. What does the term "desktop" refer to?

. How do you open a program from the desktop?

. How do you open a program such as Microsoft Word when there are not icons on the desktop?

. How do you minimize or maximize a window of a program?

. How do you close an opened program window?

. On Windows OS, how do you get to the start menu?

. What is the C: drive?

. How do you create a new folder or new file?

. How do you copy and paste text or an image onto another document or into an email?

. What are the keyboard shortcuts to "copy" and "paste"? What is the keyboard shortcut to "cut"?

. What is the file explorer on a Window OS?

. How do you change the name of a file?

. What is the difference of "Save" and "Save As"?

. Name two popular web browsers.

. What is a URL and where is it located in a web browser?

. In Google Chrome, how do you inspect a web page?
